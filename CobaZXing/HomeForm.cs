﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CobaZXing
{
    public partial class HomeForm : Form
    {
        public HomeForm()
        {
            InitializeComponent();
        }

        private void btnGenerateQRCode_Click(object sender, EventArgs e)
        {
            var generatorForm = new QRCodeGeneratorForm();
            generatorForm.FormClosed += 
            (s, evt) =>
            {
                this.Show();
            };

            this.Hide();
            generatorForm.Show();
        }

        private void btnReadQRCode_Click(object sender, EventArgs e)
        {
            var readerForm = new QRCodeReaderForm();
            readerForm.FormClosed +=
            (s, evt) =>
            {
                this.Show();
            };
            this.Hide();
            readerForm.Show();
        }
    }
}
